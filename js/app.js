import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getStorage} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";


  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyA0yJ2yWETXLPB_NK-AKy8XSDP8bR-9yoE",
    authDomain: "adminweb-af83c.firebaseapp.com",
    projectId: "adminweb-af83c",
    storageBucket: "adminweb-af83c.appspot.com",
    messagingSenderId: "54245600682",
    appId: "1:54245600682:web:a03a03e3d30c5cf5580fd3"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  export {app}
  export const auth = getAuth(app);
  export const storage = getStorage(app)