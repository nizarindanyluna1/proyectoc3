import { ref, get, getDatabase } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { app } from './app.js';
const db = getDatabase(app);

// Obtener referencia a "Productos"
const productosRef = ref(db, 'Productos');
articulos();
// Obtener los datos de "Productos"
get(productosRef).then((snapshot) => {
  if (snapshot.exists()) {
    const productosData = snapshot.val();
    //Cantidad de Datos
    const numNodos = Object.keys(productosData).length; 
    for (let x = 0; x < numNodos; x++) {
      const nodoProductoRef = ref(db, 'Productos/'+x);
      get(nodoProductoRef).then((snapshot) => {
        const data = snapshot.val();
        //-----------Agregando Imagenes-----------
        var cards = document.querySelectorAll('.card'); 
        var imgElement = cards[x].querySelector('.card-img-top'); 
        imgElement.src = "https://firebasestorage.googleapis.com/v0/b/proyectoweb-f2ea4.appspot.com/o/img%2F1.png?alt=media&token=c46ed9ef-51ae-4cdf-ac78-020e2d187b5c"; 
        //-----------Agregando Titulo-----------
        var cardBody = document.querySelectorAll('.card-body'); 
        var title = cardBody[x].querySelector('.card-title'); 
        title.textContent = data.nombre;
        //-----------Agregando Precio-----------
        var precio = cardBody[x].querySelector('.card-text'); 
        precio.textContent = "$"+data.precio; 
      }).catch((error) => {
        console.error('Error al obtener datos del subnodo '+x+' :, ' +error);
      });
    }
  } else {
    console.log("El nodo 'Productos' no contiene datos.");
  }
}).catch((error) => {
  console.error("Error al obtener datos de 'Productos':", error);
});

function articulos(){
  const section = document.getElementById('cuerpo');
  // Crear una nueva etiqueta div
  const article = document.createElement('article');
  //Agregar clases a la etiqueta   
  article.classList.add('d-flex', 'mb-3');
  //Añadir la etiqueta al section
  const div1 = document.createElement('div');
  div1.classList.add('d-flex', 'justify-content-center','text-center');
  const div2 = document.createElement('div');
  div2.classList.add('card', 'text-bg-dark','mb-3', 'ms-3', 'me-3', 'p-1');
  const img = document.createElement('img');
  div2.style.width = '30%';
  img.classList.add('card-img-top', 'rounded-5', 'img-fluid');
  const div3 = document.createElement('div');
  
  section.appendChild(article);
  article.appendChild(div1);
  div1.appendChild(div2);
  div2.appendChild(img);

}