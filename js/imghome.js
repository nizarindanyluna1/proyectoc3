import { ref, get, getDatabase } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { app } from './app.js';
const db = getDatabase(app);

// Obtener referencia a "Productos"
const productosRef = ref(db, 'Productos');
// Obtener los datos de "Productos"
get(productosRef).then((snapshot) => {
  if (snapshot.exists()) {
    const productosData = snapshot.val();
    //Cantidad de Datos
    const numNodos = Object.keys(productosData).length; 
    for (let x = 0; x < numNodos; x++) {
      const nodoProductoRef = ref(db, 'Productos/'+x);
      get(nodoProductoRef).then((snapshot) => {
        const data = snapshot.val();
        //-----------Agregando Imagenes-----------
        var cards = document.querySelectorAll('.card'); 
        var imgElement = cards[x].querySelector('.card-img');   
        imgElement.src = "https://firebasestorage.googleapis.com/v0/b/adminweb-af83c.appspot.com/o/libros%2FCaminoreyes.jpg?alt=media&token=da7cf9df-a4fd-4cbd-9ed8-d7fdaf416d80";  
    })
    }
  } else {
    console.log("");
  }
})